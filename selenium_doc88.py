#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
@Project ：demo 
@File    ：selenium_doc88.py
@IDE     ：PyCharm 
@Author  ：Sam 王大壮
@Date    ：2024-1-19 14:39 
"""
import os
import random
import time

from lxml import etree
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
# 0827 导入驱动程序包
from webdriver_manager.chrome import ChromeDriverManager


def get_doc88_data(data_url):
    # 判断文件夹是否存在，不存在创建文件夹
    path = os.getcwd() + '\data'
    is_exists = os.path.exists(path)
    if not is_exists:
        os.mkdir(path)
    # 驱动初始化
    # 指定浏览器下载文件夹
    prefs = {"download.default_directory":path,"profile.default_content_setting_values.automatic_downloads":1}
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_experimental_option("prefs", prefs)
    chrome_options.add_argument(
        'user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36')

    driver_path = ChromeDriverManager().install()
    service = Service(driver_path)
    driver = webdriver.Chrome(service=service, options=chrome_options)
    driver.set_window_size(1440, 900)
    time.sleep(float(random.randint(1000, 2000) / 1000))
    driver.get(f'{data_url}')
    windows = driver.window_handles
    driver.switch_to.window(windows[-1])
    # 网页源码
    text = driver.page_source
    html = etree.HTML(text)
    page_num = html.xpath("//li[@class='text']/text()")[0]
    # #获取总页码数
    page_num = int(page_num.replace('/ ', ''))
    print(f'共{page_num}页')
    print(EC.visibility_of_element_located((By.XPATH, "//div[@id='continueButton']")))
    # #等待网页加载
    time.sleep(10)
    # 等待按钮
    element = WebDriverWait(driver, 20).until(
        EC.visibility_of_element_located((By.XPATH, "//div[@id='continueButton']")))
    element.click()
    js = "return action=document.body.scrollHeight"
    # 初始化现在滚动条所在高度为0
    height = 0
    # 当前窗口总高度
    new_height = driver.execute_script(js)
    k = 0
    # 下载
    while k <= page_num:
        for i in range(height, new_height, 3000):
            k += 1
            driver.execute_script('window.scrollTo(0, {})'.format(i))
            time.sleep(1)
            a = f"downloadPages({k}, {k})"
            # 中间需要手动点一下运行下载多个文件
            with open('./js.js', 'r', encoding='utf-8') as f:
                _js = f.read()
            driver.execute_script(_js + a)

    input()
    pass


def main():
    # https://www.doc88.com/p-74687670324670.html
    # https://www.doc88.com/p-549550988532.html
    data_url = 'https://www.doc88.com/p-74687670324670.html'
    get_doc88_data(data_url)
    pass


if __name__ == '__main__':
    main()
